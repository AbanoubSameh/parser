#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "headers/functions.h"

// those are the possible values for gates
const char *const_tokens[] = {
    "MOV",

    "ADD",
    "SUB",
    "MUL",
    "DIV",
    "MOD",

    "CAND",
    "COR",

    "AND",
    "OR",
    "XOR",
    "NOT",

    "SHL",
    "SHR",
    "ROL",
    "ROR",

    "CMP",
    "MOVE",
    "MOVNE",
    "MOVST",
    "MOVGT",
    "MOVSE",
    "MOVGE",

    "INC",
    "DEC",
    "INC",
    "DEC",

    "JMP",
    "JE",
    "JNE",
    "JST",
    "JGT",
    "JSE",
    "JGE",

    "MOV",
    "PLAM",
    "MIAM",
    "MUAM",
    "DIAM",
    "MOAM",
    "SLAM",
    "SRAM",
    "RLAM",
    "RRAM",
    "ANAM",
    "ORAM",
    "NOAM",

    "ARRAYM",
    "ARRAYPM",
    "POINTM",
    "REFM",
    "PREFM",
    "DOTM",

    "INT_TM",
    "DBL_TM",
    "CHR_TM",
    "IF",
    "FOR",
    "WHILE",
    "DO_WHILE",
    "END_OF_BODY",
    "FUNCTION",
    "ARGUMENT",
    "ARGUMENTS",
    "DELARATION",
};

// some functions to duplicate stuff
char *strduplic(char* str) {
    char *new_str = malloc(strlen(str) + 1);
    if (new_str == NULL) {
        return NULL;
    }
    strcpy(new_str, str);
    return new_str;
}

int *intduplic(int *num) {
    int *new_int = malloc(sizeof(int));
    if (new_int == NULL) {
        return NULL;
    }
    *new_int = *num;
    return new_int;
}

double *dblduplic(double *dbl) {
    double *new_dbl = malloc(sizeof(double));
    if (new_dbl == NULL) {
        return NULL;
    }
    *new_dbl = *dbl;
    return new_dbl;
}

// print a number in binary form
void print_bin(unsigned long bin, int size) {
    int size_in_bits = size * 8;
    unsigned long temp;
    for (int i = size_in_bits - 1; i >= 0; i--) {
        temp = bin & ((unsigned long) pow(2, i));
        temp >>= i;
        printf("%lu", temp);
    }
}

// my own implementation of rotate which should be used by the
// >>> and <<< operators
long rotate_left(unsigned long number, int to_rotate, int size) {
    int size_in_bits = size * 8;
    unsigned long to_copy;
    unsigned long temp;
    unsigned long to_return = number;
    if (to_rotate > size_in_bits) {
        to_rotate = to_rotate % size_in_bits;
    }
    if (to_rotate > 0) {
        to_copy = (unsigned long) pow(2, to_rotate) - 1;
        to_copy = to_copy << (size_in_bits - to_rotate);
        temp = number & to_copy;
        temp = temp >> (size_in_bits - to_rotate);
        to_return = to_return << to_rotate;
        to_return = to_return | temp;
    }
    return to_return;
}

long rotate_right(unsigned long number, int to_rotate, int size) {
    int size_in_bits = size * 8;
    unsigned long to_copy;
    unsigned long temp;
    unsigned long to_return = number;
    if (to_rotate > size_in_bits) {
        to_rotate = to_rotate % size_in_bits;
    }
    if (to_rotate > 0) {
        to_copy = (unsigned long) pow(2, to_rotate) - 1;
        temp = number & to_copy;
        temp = temp << (size_in_bits - to_rotate);
        to_return = to_return >> to_rotate;
        to_copy = (unsigned long) pow(2, size_in_bits - to_rotate) - 1;
        to_return = to_return & to_copy;
        to_return = to_return | temp;
    }
    return to_return;
}

// this is the number of registers that the target machine
#define NUM_OF_REGS 32

bool registers[NUM_OF_REGS];

int get_register() {
    // returns the first free register
    for (int i = 0; i <= NUM_OF_REGS; i++) {
        if (!registers[i]) {
            registers[i] = true;
            return i;
        }
    }
    printf("no more registers\n");
    return -1;
}

// prints the states of registers
void print_registers() {
    for (int i = 0; i < NUM_OF_REGS; i++) {
        printf("reg%d:%d\n", i, registers[i]);
    }
}

// used to free a register
void free_register(int reg) {
    registers[reg] = false;
}

// keeps track of the number of blocks
int num_of_blocks = 1;

int get_block() {
    return num_of_blocks++;
}

// keeps track of the number of labels
int num_of_labels = 1;

int get_label() {
    return num_of_labels++;
}

// takes an int and returns its string value
// from const_tokens
const char *get_token(int num) {
    return const_tokens[num];
}

// those functions are used for printing
const char *SP1 = "\t\t";
const char *SP2 = "\t\t";
const char *SP3 = "\t\t";

const char *op_type[] = { "r", "l", "b", "#" };

void print_other(int other) {
    if (other == 1) printf("\n");
    else  if (other == 2) printf(",%s", SP3);
}

void print_uni_op_inst(int inst, int op1, int op1_type, int other) {
    printf("%s%s%s%s%d", SP1, get_token(inst), SP2, op_type[op1_type], op1);
    print_other(other);
}

void print_bin_op_inst(int inst, int op1, int op1_type, int op2, int op2_type, int other) {
    printf("%s%s%s%s%d,%s%s%d", SP1, get_token(inst), SP2, op_type[op1_type], op1, SP3, op_type[op2_type], op2);
    print_other(other);
}

void print_label(int label, int other) {
    printf("%s%d:", op_type[LBL], label);
    print_other(other);
}

void print_string(char *var, int other) {
    printf("%s", var);
    print_other(other);
}

void print_int(int num, int other) {
    printf("%s%d", op_type[NUM], num);
    print_other(other);
}

void print_double(double num, int other) {
    printf("%s%.9f", op_type[NUM], num);
    print_other(other);
}
