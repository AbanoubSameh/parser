#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include "headers/functions.h"
#include "headers/parser.h"
#include "headers/Operand.h"
#include "headers/LinkedList.h"
#include "headers/Stack.h"

// stack would be used for functions and statements
Stack *scope;

// this variable contains the number of the current line
extern int line_count;

// this should contain the tree returned after a call to parser
Operand *Ast;

// this should be a table containing all declared variables
LinkedList *varList;
LinkedList *literals;

// this gets called before we exit
void cleanup(int exit_int) {
    printf("cleaning up\n");
    if (Ast != NULL && Ast != 0) {
        // to make sure we free all heap allocations
        delete_Operand(Ast);
    }
    delete_Stack(scope);
    delete_LinkedList(varList);
    delete_LinkedList(literals);
    // this exit value to return to the system
    exit(exit_int);
}

// this should be called at the beginning of the program
void init() {
    // this makes sure we run cleanup even if enterrupted
    signal(SIGINT, cleanup);
    Ast = NULL;         // initialize the Ast
    scope = new_Stack();
    push(scope, 0);
    varList = new_LinkedList();     // and also the variable list
    literals = new_LinkedList();
    //memset(stack, 0, 256);
}

int main() {
    init();

    //printf("%s\n", get_token(34));

    int ret_code;   // THIS SHOULD BE OBSOLETE

    //printf("main:\n");
    // yyparse is made by bison and should parse only one instruction
    // at a time, that's why we have a loop
    while ((ret_code = yyparse())) {
#ifdef PGENMAIN
        printf("an instruction has been parsed\n");
#endif
        // NEED TO GET RID OF THIS AT A LATER POINT
        if (ret_code == 1) {
            print_Operand(Ast);
        } else {
            printf("at line: %d, can't redefine variable", line_count);
            cleanup(5);
        }
        delete_Operand(Ast);
        Ast = NULL;
        //print_last_node(VarList);
    }

    cleanup(0);
}
