// This file has been created on February-25-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "headers/functions.h"
#include "headers/var_functions.h"
#include "headers/LinkedList.h"
#include "headers/Literal.h"

extern LinkedList *varList;
extern LinkedList *literals;
void cleanup(int);

Variable *add_to_varList(int type, int pointer, int array, int function, char *name, void *scope_args) {
    int ret_value = exists_in_varList(function, name, scope_args);
    if ((ret_value == 1 && function)) {
        printf("redefinition of variable %s in scope\n", name);
        printf("scope_args: %ld\n", (long) scope_args);
        printf("%d\n", ret_value);
        cleanup(13);
    } else if (ret_value == 2 && !function) {
        printf("redefinition of function %s\n", name);
        printf("scope_args: %ld\n", (long) scope_args);
        printf("%d\n", ret_value);
        cleanup(13);
    }
    //printf("scope_args: %ld\n", (long) scope_args);
    Variable *var = new_Variable(type, pointer, array, function, name, scope_args);
    add_to_start_of_LinkedList(varList, var);
    // this function is of type int because later I might add some checks before
    // creating the new variable, in which case it will need to return a number
    // to indicate failure
    return var;
}

int add_to_literals(char *str) {
    int exists = 0;
    if ((exists = exists_in_literals(str)) != 0) return exists;
    int label = get_label();
    Literal *lit = new_Literal(label, str);
    add_to_start_of_LinkedList(literals, lit);
    // this function is of type int for the same reason as the add_to_varList function
    return label;
}

int exists_in_varList(int function, char *name, void *scope_args) {
    Variable *temp;
    if (function) {
        for (int i = 1; i <= varList->num_of_nodes; i++) {
            temp = get_nth_object_in_LinkedList(varList, i);
            if (!strcmp(temp->name, name)) {
#               ifdef PGENVARFUN
                printf("function %s exists\n", name);
#               endif
                return 1;
            }
        }
    } else {
        int found = 0;
        for (int i = 1; i <= varList->num_of_nodes; i++) {
            temp = get_nth_object_in_LinkedList(varList, i);
            if (!strcmp(temp->name, name)) {
                if (temp->scope_args == scope_args) {
#                   ifdef PGENVARFUN
                    printf("variable %s exists with scope %ld\n", name, (long) scope_args);
#                   endif
                    return 2;
                } else {
                    found = 1;
                }
            }
        }
        if (found) {
#           ifdef PGENVARFUN
            printf("variable %s exists with scope %ld\n", name, (long) temp->scope_args);
#           endif
            return 3;
        }
    }
#   ifdef PGENVARFUN
    printf("variable with name %s does not exist\n", name);
#   endif
    return 0;
}

int exists_in_literals(char *str) {
    Literal *temp;
    for (int i = 1; i <= literals->num_of_nodes; i++) {
        temp = get_nth_object_in_LinkedList(literals, i);
        if (!strcmp(temp->str, str)) {
#           ifdef PGENVARFUN
            printf("literal %s exists\n", str);
#           endif
            return temp->label;
        }
    }
#   ifdef PGENVARFUN
    printf("literal %s does not exist\n", str);
#   endif
    return 0;
}

Variable *get_latest_var(char *name) {
    Variable *temp;
    for (int i = 1; i <= varList->num_of_nodes; i++) {
        temp = get_nth_object_in_LinkedList(varList, i);
        if (!strcmp(temp->name, name)) {
#           ifdef PGENVARFUN
            printf("found variable with name %s with scope %ld\n", name, (long) temp->scope_args);
#           endif
            return temp;
        }
    }
#   ifdef PGENVARFUN
    printf("variable with name %s not found\n", name);
#   endif
    return NULL;
}
