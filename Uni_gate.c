// This file has been created on February-09-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>

#include "headers/functions.h"
#include "headers/Operand.h"
#include "headers/Uni_gate.h"
#include "headers/Stack.h"

void cleanup(int);
extern Stack *scope;

// Uni_gate constructor with type and operand
Uni_gate* new_cUni_gate(Operand *op1, uint8_t type) {
    Uni_gate *gate = malloc(sizeof(Uni_gate));
    gate->type = type;
    gate->operand1 = op1;
#   ifdef PGENUN
    printf("gate created at: %ld with type: %s, op1: ", (long) gate, get_token(type));
    print_op(op1);
    printf("\n");
#   endif
    return gate;
}

// Uni_gate constructor with no input
Uni_gate* new_Uni_gate() {
    Uni_gate *gate = malloc(sizeof(Uni_gate));
    gate->operand1 = NULL;
#   ifdef PGENUN
    printf("gate created at: %ld\n", (long) gate);
#   endif
    return gate;
}

// setters for the gate
void set_Ugate_type(Uni_gate *gate, uint8_t type) {
    gate->type = type;
#   ifdef PGENUN
    printf("gate at %ld has type: %d\n", (long) gate, type);
#   endif
}

void set_Ugate_operand1(Uni_gate *gate, Operand *op) {
    gate->operand1 = op;
#   ifdef PGENUN
    printf("gate at %ld has op1: %ld\n", (long) gate, (long) op);
#   endif
}

// and destructor
void delete_Uni_gate(Uni_gate *gate) {
    if (gate == NULL) return;
#   ifdef PDEL
    printf("deleting a uni_gate\n");
#   endif
    if (gate->operand1 != NULL) delete_Operand(gate->operand1);
    free(gate);
}

int solve_if_stmt(Uni_gate *root) {
    if ((long) peek(scope) == 0) {
        printf("can't have a statement outside a function\n");
        cleanup(15);
    }
    int reg1 = -1;
    int blk1 = get_label();
    push(scope, new_Operand(&blk1, INTP));
    printf("the scope has been changed to B%d\n", blk1);
    if (root->operand1->type == BINP || root->operand1->type == UNIP) {
        // if the operand contains another gate, print it fisrt
        reg1 = print_Operand(root->operand1);
        printf("CMP\tr%d,\t0\n", reg1);
    } else {
        printf("CMP\t");
        print_Operand(root->operand1);
        printf("\t,0\n");
    }
    printf("je\tl%d\n", blk1);

    //print_stack();
    return reg1;
}

int end_of_body() {
    if ((long) peek(scope) == 0) {
        printf("expecting opening brace before closing brace\n");
        cleanup(8);
        return 0;
    }
    Operand *block = pop(scope);
    if (block->type == INTP) {
        //if (block != 0) printf("l%d\n", block);
        printf("old scope: %d\n", *(int*) block->content);
    } else if (block->type == STRP) {
        printf("end of function %s\n", (char*) block->content);
    } else {
        printf("unknown error occurred at the end of body\n");
        cleanup(14);
    }
    //else printf("end of a function\n");
    return 1;
}

// this is actually used to generate the code
int print_Uni_gate(Uni_gate *root) {
    int state = 0, reg1 = -1;

    if (root->type == END_OF_BODY) return end_of_body();
    if (root->type == IF_STMT) return solve_if_stmt(root);

    // ALL THOSE NEED TO BE OBSOLETE
    // NEED TO PRINT EVERYTHING FROM ONE PLACE
    const char* sp1 = "\t\t";
    const char* sp2 = "\t\t";
    const char* sp3 = "\t\t";

    if (root->operand1->type == BINP || root->operand1->type == UNIP) {
        // if the operand contains another gate, print it fisrt
        reg1 = print_Operand(root->operand1);
        state = 1;
    }

    if (state == 0) {
        // if this is the first thing to be printed:
        // get a new register
        reg1 = get_register();
        // then do the operations

        //printf("%s%s%sr%d,%s", sp1, get_token(MOVM), sp2, reg1, sp3);
        print_uni_op_inst(MOVM, reg1, REG, 2);
        print_Operand(root->operand1);
        printf("\n");

        //printf("%s%s%sr%d", sp1, get_token(root->type), sp2, reg1);
        print_uni_op_inst(root->type, reg1, REG, 2);
        printf("\n");
        //return reg1;
    } else if (state == 1) {
        // if something has already been printed
        // then use the give register

        //printf("%s%s%sr%d\n", sp1, get_token(root->type), sp2, reg1);
        print_uni_op_inst(root->type, reg1, REG, 1);
        //return reg1;
    } else {
        // in case of a problem cleanup
        printf("Unknown error\n");
        cleanup(4);
        return -1;
    }

    // those are special cases for increment and decrement
    if (root->type == INCAM || root->type == DECAM) {
        // need a new register to hold the value
        int reg2 = get_register();
        // after we increment, we do not write the value
        // except after we use the old one for our operation

        //printf("%s%s%sr%d,%s", sp1, get_token(MOVM), sp2, reg2, sp3);
        print_uni_op_inst(MOVM, reg2, REG, 2);
        print_Operand(root->operand1);
        printf("\n");

        // ONLY HERE NEEDS TO BE DELT WITH
        printf("%s%s%s", sp1, get_token(MOVM), sp2);
        print_Operand(root->operand1);
        printf(",%sr%d\n", sp3, reg1);

        return reg2;
    }

    if (root->type == INCFM || root->type == DECFM) {
        // this is easier because we can increment then use the value
        // immediately for the next operation
        printf("%s%s%s", sp1, get_token(MOVM), sp2);
        print_Operand(root->operand1);
        printf(",%sr%d\n", sp3, reg1);
    }

    return reg1;
    //print_Operand(root->operand1);
}
