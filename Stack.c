// This file has been created on March-22-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>

#include "headers/Stack.h"

// private functions used bt other functions
static int create_only_Node(Stack *stack, void *obj) {
    stack->top = new_Node(obj);
    stack->top->next = NULL;
    return 1;
}

static int exists(Stack *stack) {
    if (stack != NULL) return 1;
    return 0;
}

Stack *new_Stack() {
    Stack *stack = malloc(sizeof(Stack));
    stack->top = NULL;
    return stack;
}

int delete_Stack(Stack *stack) {
    if (!exists(stack)) {
        printf("can't delete a stack that does not exist\n");
        return 0;
    }

    printf("deleting a stack\n");
	int count = 0;
	Node *temp = stack->top;
    Node *temp2;

    // just deletes the stack by deleting the Nodes
	while (temp != NULL) {
		temp2 = temp->next;
		//free(temp);
        delete_Node(temp, DELETE_OBJECT);
		temp = temp2;
		count++;
	}

    free(stack);
	printf("%d Nodes freed\n", count);
    return 1;
}

int Stack_is_empty(Stack *stack) {
    if (!exists(stack)) {
        printf("can't find out if a stack is empty if it does not exist\n");
        return 0;
    }

    if (stack->top == NULL) {
        //printf("stack is empty\n");
        return 1;
    }
    return 0;
}

int empty_stack(Stack *stack) {
    if (!exists(stack)) {
        printf("can't empty a stack that does not exist\n");
        return 0;
    }

    // is stack exists delete it and make a new one
    delete_Stack(stack);
    stack = new_Stack();
    return 1;
}

int push(Stack *stack, void *obj) {
    if (!exists(stack)) {
        printf("can't push to a stack that does not exist\n");
        return 0;
    }

    // if stack is empty create first node
    if (Stack_is_empty(stack)) return create_only_Node(stack, obj);

    // else:

    // create a new node
    Node *temp = new_Node(obj);
    // set its next node as the stack's top
    temp->next = stack->top;
    // and then set the stack's top as the new node
    stack->top = temp;
    return 1;
}

void *pop(Stack *stack) {
    if (!exists(stack)) {
        printf("can't pop from a stack that does not exist\n");
        return NULL;
    }

    // if stack is empty return NULL
    if (Stack_is_empty(stack)) return NULL;

    // else:

    // get hold of the object at the top node
    Node *temp = stack->top;
    void *toReturn = temp->obj;
    // set the top to the node right before it
    stack->top = stack->top->next;
    // delete the node
    delete_Node(temp, DO_NOT_DELETE_OBJECT);
    // return the top
    return toReturn;
}

void *peek(Stack *stack) {
    if (!exists(stack)) {
        printf("can't peek at a stack that does not exist\n");
        return NULL;
    }

    // if stack is empty return NULL
    if (Stack_is_empty(stack)) return NULL;
    return stack->top->obj;
}

void *get_value_at_base(Stack *stack) {
    if (!exists(stack)) {
        printf("can't peek at a stack that does not exist\n");
        return NULL;
    }

    // if stack is empty return NULL
    if (Stack_is_empty(stack)) return NULL;

    Node *temp = stack->top;
    while (temp->next != NULL) {
        temp = temp->next;
    }
    return temp->obj;
}
