%{
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "headers/functions.h"
#include "headers/Operand.h"
#include "headers/Bin_gate.h"
#include "headers/Uni_gate.h"
#include "headers/Variable.h"
#include "headers/var_functions.h"

#define YYSTYPE Operand*
void cleanup(int);
int yylex();

extern int line_count;
extern int letter_count;
extern int yyleng;

void yyerror(const char *str) {
    printf("%s at line: %d and letter: %d\n", str, line_count, letter_count - yyleng);
    cleanup(2);
}

int yywrap() {
    return 1;
}

extern Operand *Ast;

int TTRUE = 1, TFALSE = 0;
int BYT_T = 0, SHT_T = 1, INT_T = 2, LNG_T = 3, FLT_T = 4, DBL_T = 5, CHR_T = 6;

%}

%start instruction

%token IF ELSE FOR WHILE DO STR_LIT
%token VID_ID INT_ID DBL_ID CHR_ID
%token INT DBL STR ARR DOT END SMC CMA
%token OPR CPR OBR CBR OSB CSB
%token ADD SUB MUL DIV MOD INC DEC
%token AND ORR BND BOR BXR
%token EQU NEQ SEQ GEQ STH GTH NOT
%token SHR SHL ROR ROL TIL
%token ASS PLA MIA MUA DIA MOA
%token SLA SRA RLA RRA ANA ORA NOA

%%

argument
    : INT_ID variable                       { $$ = new_Operand(new_cBin_gate($1, $2, ARG), BINP); }
    | DBL_ID variable                       { $$ = new_Operand(new_cBin_gate($1, $2, ARG), BINP); }
    | CHR_ID variable                       { $$ = new_Operand(new_cBin_gate($1, $2, ARG), BINP); }
    | INT_ID                                { $$ = new_Operand(new_cBin_gate($1,
                                                new_Operand(&TFALSE, INTP), ARG), BINP); }
    | DBL_ID                                { $$ = new_Operand(new_cBin_gate($1,
                                                new_Operand(&TFALSE, INTP), ARG), BINP); }
    | CHR_ID                                { $$ = new_Operand(new_cBin_gate($1,
                                                new_Operand(&TFALSE, INTP), ARG), BINP); }
    ;

arguments
    :                                       { $$ = new_Operand(&TFALSE, INTP); }
    | argument
    | arguments CMA argument                { $$ = new_Operand(new_cBin_gate($1, $3, ARGS), BINP); }
    ;

array
    :                                       { $$ = NULL; }
    | INT
    ;

variable
    : MUL variable                          { $$ = new_Operand(new_cUni_gate($2, POINTM), UNIP); }
    | BND variable                          { $$ = new_Operand(new_cUni_gate($2, REFM), UNIP); }
    | STR OSB array CSB                     { $$ = new_Operand(new_cBin_gate($1, $3, ARRAYM), BINP); }
    | STR OBR arguments CBR                 { $$ = new_Operand(new_cBin_gate($1, $3, ARGS), BINP); }
    | STR
    ;

postfix
    : OPR assignment CPR                    { $$ = $2; }
    | variable
    | INT
    | DBL
    | STR_LIT
    | INC STR                               { $$ = new_Operand(new_cUni_gate($2, INCFM), UNIP); }
    | DEC STR                               { $$ = new_Operand(new_cUni_gate($2, DECFM), UNIP); }
    | STR INC                               { $$ = new_Operand(new_cUni_gate($1, INCAM), UNIP); }
    | STR DEC                               { $$ = new_Operand(new_cUni_gate($1, DECAM), UNIP); }
    | STR ARR                               { $$ = new_Operand(new_cUni_gate($1, PREFM), UNIP); }
    | STR DOT                               { $$ = new_Operand(new_cUni_gate($1, DOTM), UNIP); }
    ;

unary
    : TIL unary                             { $$ = new_Operand(new_cUni_gate($2, TILM), UNIP); }
    | NOT unary                             { $$ = new_Operand(new_cBin_gate(new_Operand(new_cBin_gate(
                                                new_Operand(new_cBin_gate($2, new_Operand(&TFALSE, INTP),
                                                CMPM), BINP), new_Operand(&TTRUE, INTP), MOVEM), BINP),
                                                new_Operand(&TFALSE, INTP), MOVNEM), BINP); }
    | SUB unary                             { $$ = new_Operand(new_cUni_gate($2, SUBM), UNIP); }
    | postfix
    ;

multiplicative
    : multiplicative MUL unary              { $$ = new_Operand(new_cBin_gate($1, $3, MULM), BINP); }
    | multiplicative DIV unary              { $$ = new_Operand(new_cBin_gate($1, $3, DIVM), BINP); }
    | multiplicative MOD unary              { $$ = new_Operand(new_cBin_gate($1, $3, MODM), BINP); }
    | unary
    ;

additive
    : additive ADD multiplicative           { $$ = new_Operand(new_cBin_gate($1, $3, ADDM), BINP); }
    | additive SUB multiplicative           { $$ = new_Operand(new_cBin_gate($1, $3, SUBM), BINP); }
    | multiplicative
    ;

shift
    : shift SHR additive                    { $$ = new_Operand(new_cBin_gate($1, $3, SHRM), BINP); }
    | shift SHL additive                    { $$ = new_Operand(new_cBin_gate($1, $3, SHLM), BINP); }
    | additive
    ;

relational
    : relational STH shift                  { $$ = new_Operand(new_cBin_gate(new_Operand(new_cBin_gate(
                                                new_Operand(new_cBin_gate($1, $3, CMPM), BINP),
                                                new_Operand(&TTRUE, INTP), MOVSTM), BINP),
                                                new_Operand(&TFALSE, INTP), MOVGEM), BINP); }
    | relational GTH shift                  { $$ = new_Operand(new_cBin_gate(new_Operand(new_cBin_gate(
                                                new_Operand(new_cBin_gate($1, $3, CMPM), BINP),
                                                new_Operand(&TTRUE, INTP), MOVGTM), BINP),
                                                new_Operand(&TFALSE, INTP), MOVSEM), BINP); }
    | relational SEQ shift                  { $$ = new_Operand(new_cBin_gate(new_Operand(new_cBin_gate(
                                                new_Operand(new_cBin_gate($1, $3, CMPM), BINP),
                                                new_Operand(&TTRUE, INTP), MOVSEM), BINP),
                                                new_Operand(&TFALSE, INTP), MOVGTM), BINP); }
    | relational GEQ shift                  { $$ = new_Operand(new_cBin_gate(new_Operand(new_cBin_gate(
                                                new_Operand(new_cBin_gate($1, $3, CMPM), BINP),
                                                new_Operand(&TTRUE, INTP), MOVGEM), BINP),
                                                new_Operand(&TFALSE, INTP), MOVSTM), BINP); }
    | shift
    ;

equality
    : equality EQU relational               { $$ = new_Operand(new_cBin_gate(new_Operand(new_cBin_gate(
                                                new_Operand(new_cBin_gate($1, $3, CMPM), BINP),
                                                new_Operand(&TTRUE, INTP), MOVEM), BINP),
                                                new_Operand(&TFALSE, INTP), MOVNEM), BINP); }
    | equality NEQ relational               { $$ = new_Operand(new_cBin_gate(new_Operand(new_cBin_gate(
                                                new_Operand(new_cBin_gate($1, $3, CMPM), BINP),
                                                new_Operand(&TTRUE, INTP), MOVNEM), BINP),
                                                new_Operand(&TFALSE, INTP), MOVEM), BINP); }
    | relational
    ;

bitwise_and
    : bitwise_and BND equality              { $$ = new_Operand(new_cBin_gate($1, $3, BANDM), BINP); }
    | equality
    ;

bitwise_xor
    : bitwise_xor BXR bitwise_and           { $$ = new_Operand(new_cBin_gate($1, $3, BXORM), BINP); }
    | bitwise_and
    ;

bitwise_or
    : bitwise_or BOR bitwise_xor            { $$ = new_Operand(new_cBin_gate($1, $3, BORM), BINP); }
    | bitwise_xor
    ;

and
    : and AND additive                      { $$ = new_Operand(new_cBin_gate($1, $3, ANDM), BINP); }
    | bitwise_or
    ;

or
    : or ORR and                            { $$ = new_Operand(new_cBin_gate($1, $3, ORM), BINP); }
    | and
    ;

assignment
    : STR ASS assignment                    { $$ = new_Operand(new_cBin_gate($1, $3, ASSM), BINP); }
    | or
    ;

declaration
    : INT_ID variable SMC                   { $$ = new_Operand(new_cBin_gate(new_Operand(new_cBin_gate(new_Operand(&INT_T, INTP), $2, DECLM), BINP), new_Operand(&TFALSE, INTP), ASSM), BINP); }
    | DBL_ID variable SMC                   { $$ = new_Operand(new_cBin_gate(new_Operand(new_cBin_gate(new_Operand(&DBL_T, INTP), $2, DECLM), BINP), new_Operand(&TFALSE, INTP), ASSM), BINP); }
    | CHR_ID variable SMC                   { $$ = new_Operand(new_cBin_gate(new_Operand(new_cBin_gate(new_Operand(&CHR_T, INTP), $2, DECLM), BINP), new_Operand(&TFALSE, INTP), ASSM), BINP); }
    | INT_ID variable ASS assignment SMC    { $$ = new_Operand(new_cBin_gate(new_Operand(new_cBin_gate(new_Operand(&INT_T, INTP), $2, DECLM), BINP), $4, ASSM), BINP); }
    | DBL_ID variable ASS assignment SMC    { $$ = new_Operand(new_cBin_gate(new_Operand(new_cBin_gate(new_Operand(&DBL_T, INTP), $2, DECLM), BINP), $4, ASSM), BINP); }
    | CHR_ID variable ASS assignment SMC    { $$ = new_Operand(new_cBin_gate(new_Operand(new_cBin_gate(new_Operand(&CHR_T, INTP), $2, DECLM), BINP), $4, ASSM), BINP); }
    ;

if
    : IF OPR assignment CPR OBR             { $$ = new_Operand(new_cUni_gate($3, IF_STMT), UNIP); }
    ;

statement
    : if
    ;

function
    : INT_ID STR OPR arguments CPR OBR      { $$ = new_Operand(new_cBin_gate($2, $4, FUNC), BINP); }
    | DBL_ID STR OPR arguments CPR OBR      { $$ = new_Operand(new_cBin_gate($2, $4, FUNC), BINP); }
    | CHR_ID STR OPR arguments CPR OBR      { $$ = new_Operand(new_cBin_gate($2, $4, FUNC), BINP); }
    ;

end_of_body
    : CBR                                   { $$ = new_Operand(new_cUni_gate(NULL, END_OF_BODY), UNIP); }
    ;

instruction
    : END                                   { puts("empty, done"); return 0; }
    | assignment SMC                        { Ast = $1; return 1; }
    | declaration                           { Ast = $1; return 1; }
    | statement                             { Ast = $1; return 1; }
    | end_of_body                           { Ast = $1; return 1; }
    | function                              { Ast = $1; return 1; }
    ;

%%
