// This file has been created on March-26-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>

#include "headers/Argument.h"
#include "headers/functions.h"
#include "headers/Variable.h"

void cleanup(int);

Argument *new_Argument(int type, int pointer, char *name) {
    Argument *arg = malloc(sizeof(Argument));
    arg->type = type;
    arg->pointer = pointer;
    arg->name = strduplic(name);
    return arg;
}

void print_arg(Argument *arg) {
    printf("Argument has type: %s", get_var_type(arg->type));
    for (int i = 0; i < arg->pointer; i++) {
        printf("*");
    }
    printf(", name: %s\n", arg->name);
}

void delete_Argument(Argument *arg) {
    if (arg == NULL) {
        printf("can't delete an argument that does not exist\n");
        return;
    }
    free(arg->name);
    free(arg);
}
