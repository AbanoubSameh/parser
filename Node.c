// This file has been created on March-22-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>

#include "headers/Node.h"

// constructor for Node type
Node *new_Node(void *obj) {
	Node *node = malloc(sizeof(Node));
    node->obj = obj;
    node->next = NULL;
	return node;
}

// destructor for Node type
int delete_Node(Node *node, int delete_object) {
    if (node == NULL) {
        printf("can't delete a Node that does not exist\n");
        return 0;
    }

    if (delete_object) {
        // code here to delete the object inside the Node
        // or just return it and let the user deal with it
    }

    free(node);
    printf("deleted a node\n");
    return 1;
}

Node *duplicate_Node(Node *node) {
    if (node == NULL) {
        printf("can't dupliate a node that does not exist\n");
        return NULL;
    }

    Node *new = malloc(sizeof(Node));
    new->obj = node->obj;
    new->next = node->next;
    return new;
}
