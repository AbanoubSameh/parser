// This file has been created on March-26-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "headers/Literal.h"
#include "headers/functions.h"

Literal *new_Literal(int label, char *str) {
    Literal *lit = malloc(sizeof(Literal));
    lit->label = label;
    lit->str = strduplic(str);
    lit->size = strlen(str) + 1;
    lit->assigned = 0;
    return lit;
}

void print_lit(Literal *lit) {
    if (lit == NULL) {
        printf("Can't print a literal that does not exist\n");
        return;
    }
    printf("Literal at label: %d and has string: %s\n", lit->label, lit->str);
}

void delete_Literal(Literal *lit) {
    if (lit == NULL) {
        printf("Can't delete a literal that does not exist\n");
        return;
    }
    free(lit->str);
    free(lit);
    return;
}
