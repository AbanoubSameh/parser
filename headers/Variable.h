// This file has been created on February-21-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _variables_h_
#define _variables_h_

typedef struct Variable {
    int type;
    int pointer;
    int array;

    int function;
    int assigned;

    char *name;
    void *scope_args;

    int memory_location;
} Variable;

Variable* new_Variable(int type, int pointer, int array,
                    int function, char *name, void *scope_args);

int print_Variable_location(Variable*);
int get_Variable_size(Variable*);
void delete_Variable(Variable*);
void print_var(Variable*);

const char *get_var_type(int);

int get_memory_location(int type, int pointer, int array, int function, void *scope);

#endif
