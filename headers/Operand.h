// This file has been created on February-09-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _operand_h_
#define _operand_h_

#include <inttypes.h>

enum OPERAND_TYPES{
    STRP,
    INTP,
    DBLP,
    UNIP,
    BINP,
    DECP,
    STR_LITP,
};

typedef struct Operand {
    uint8_t type;
    void *content;
} Operand;

Operand* new_Operand(void*, uint8_t);
void delete_Operand(Operand*);
int print_Operand(Operand*);

void print_op();

#endif
