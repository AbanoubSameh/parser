// This file has been created on March-26-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _literal_h_
#define _literal_h_

typedef struct Literal {
    int label;
    char *str;
    int size;
    int assigned;
} Literal;

Literal *new_Literal(int, char*);

void print_lit(Literal*);
void delete_Literal(Literal*);

#endif
