// This file has been created on March-22-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _stack_h_
#define _stack_h_

#include "Node.h"

typedef struct Stack {
    Node *top;
} Stack;

Stack *new_Stack();
int delete_Stack(Stack*);

int Stack_is_empty(Stack*);
int empty_Stack(Stack*);

int push(Stack*, void*);
void* pop(Stack*);
void* peek(Stack*);
void* get_value_at_base(Stack*);

#endif
