// This file has been created on March-26-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _argument_h_
#define _argument_h_

typedef struct Argument {
    int type;
    int pointer;
    char *name;
} Argument;

Argument *new_Argument(int, int, char*);
void print_arg(Argument*);
void delete_Argument(Argument*);

#endif
