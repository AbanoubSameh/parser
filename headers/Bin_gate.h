// This file has been created on February-09-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _bin_gate_h_
#define _bin_gate_h_

#include "Operand.h"

typedef struct Bin_gate {
    uint8_t type;
    Operand *operand1;
    Operand *operand2;
} Bin_gate;

Bin_gate* new_cBin_gate(Operand*, Operand*, uint8_t);
Bin_gate* new_Bin_gate();

void set_Bgate_type(Bin_gate*, uint8_t);
void set_Bgate_operand1(Bin_gate*, Operand*);
void set_Bgate_operand2(Bin_gate*, Operand*);
void delete_Bin_gate(Bin_gate*);
int print_Bin_gate(Bin_gate*);

#endif
