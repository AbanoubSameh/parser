
#ifndef _functions_h_
#define _functions_h_

enum OPERATION_TYPES {
    MOVM,

    ADDM,
    SUBM,
    MULM,
    DIVM,
    MODM,

    ANDM,
    ORM,

    BANDM,
    BORM,
    BXORM,
    TILM,

    SHLM,
    SHRM,
    ROLM,
    RORM,

    CMPM,
    MOVEM,
    MOVNEM,
    MOVSTM,
    MOVGTM,
    MOVSEM,
    MOVGEM,

    INCAM,
    DECAM,
    INCFM,
    DECFM,

    JMPM,
    JEM,
    JNEM,
    JSTM,
    JGTM,
    JSEM,
    JGEM,

    ASSM,
    PLAM,
    MIAM,
    MUAM,
    DIAM,
    MOAM,
    SLAM,
    SRAM,
    RLAM,
    RRAM,
    ANAM,
    ORAM,
    NOAM,

    ARRAYM,
    ARRAYPM,
    POINTM,
    REFM,
    PREFM,
    DOTM,

    INT_TM,
    DBL_TM,
    CHR_TM,

    IF_STMT,
    FOR_STMT,
    WHILE_STMT,
    DO_WHILE_STMT,
    END_OF_BODY,
    FUNC,
    ARG,
    ARGS,
    DECLM,
};

#define REG     0
#define LBL     1
#define BLK     2
#define NUM     3

char *strduplic(char*);
int *intduplic(int*);
double *dblduplic(double*);

void print_bin(unsigned long, int);
long rotate_left(unsigned long, int, int);
long rotate_right(unsigned long, int, int);
int get_register();
int get_block();
int get_label();
void print_registers();
void free_register(int);
const char *get_token(int);
void print_uni_op_inst(int, int, int, int);
void print_bin_op_inst(int, int, int, int, int, int);
void print_label(int, int);

void print_string(char*, int);
void print_int(int, int);
void print_double(double, int);

#endif
