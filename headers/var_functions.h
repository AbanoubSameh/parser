// This file has been created on February-25-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _var_function_h_
#define _var_function_h_

#include "Variable.h"

Variable *add_to_varList(int type, int pointer, int array,
                    int function, char *name, void *scope_args);

int add_to_literals(char *str);

int exists_in_varList(int function, char *name, void *scope_args);
int exists_in_literals(char *str);

Variable *get_latest_var(char *name);

#endif
