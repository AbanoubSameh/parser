// This file has been created on February-09-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _uni_gate_h_
#define _uni_gate_h_

#include <inttypes.h>

#include "Operand.h"

typedef struct Uni_gate {
    uint8_t type;
    Operand *operand1;
} Uni_gate;

Uni_gate* new_cUni_gate(Operand*, uint8_t);
Uni_gate* new_Uni_gate();

void set_Ugate_type(Uni_gate*, uint8_t);
void set_Ugate_operand1(Uni_gate*, Operand*);
void delete_Uni_gate(Uni_gate*);
int print_Uni_gate(Uni_gate*);

#endif
