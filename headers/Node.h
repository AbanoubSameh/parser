// This file has been created on March-22-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _node_h_
#define _node_h_

#define DELETE_OBJECT           1
#define DO_NOT_DELETE_OBJECT    0

typedef struct Node {
    void *obj;
	struct Node *next;
} Node;

Node *new_Node(void*);
int delete_Node(Node*, int);

Node *duplicate_Node(Node*);

#endif
