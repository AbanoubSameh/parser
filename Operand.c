// This file has been created on February-09-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "headers/Operand.h"
#include "headers/Uni_gate.h"
#include "headers/Bin_gate.h"
#include "headers/Variable.h"
#include "headers/LinkedList.h"
#include "headers/Stack.h"
#include "headers/functions.h"
#include "headers/var_functions.h"

extern Stack *scope;
extern LinkedList *VarList;
extern LinkedList *literals;
void cleanup(int);

// this is the constructor of Operand type
Operand* new_Operand(void *op, uint8_t type) {
    Operand *operand = malloc(sizeof(Operand));
    operand->type = type;
    if (type == STRP || type == STR_LITP) {
        operand->content = strduplic(op);
#       ifdef PGENOP
        printf("ceated a new operand at: %ld with op: %s and type string\n", (long) operand, (char*) op);
#       endif
    } else if (type == INTP) {
        operand->content = intduplic(op);
#       ifdef PGENOP
        printf("ceated a new operand at: %ld with op: %d and type int\n", (long) operand, *((int*) op));
#       endif
    } else if (type == DBLP) {
        operand->content = dblduplic(op);
#       ifdef PGENOP
        printf("ceated a new operand at: %ld with op: %f and type double\n", (long) operand, *((double*) op));
#       endif
    } else {
        operand->content = op;
#       ifdef PGENOP
        printf("created a new operand at: %ld with op at: %ld\n", (long) operand, (long) op);
#       endif
    }
    return operand;
}

// destructor for Operand type
void delete_Operand(Operand *operand) {
    if (operand == NULL || operand == 0) return;
#   ifdef PDEL
    printf("deleting an operand\n");
#   endif
    if (operand->content != NULL && operand->content != 0) {
        if (operand->type == UNIP) {
            delete_Uni_gate(operand->content);
        } else if (operand->type == BINP) {
            delete_Bin_gate(operand->content);
        } else if (operand->type == DECP) {
            delete_Variable(operand->content);
        } else {
            free(operand->content);
        }
    }
    free(operand);
}

int print_Operand(Operand *root) {
    // we need to compare the type of the operand
    // and to call the right function to printed accordingly
#   ifdef PGENOP
    printf("printing operand at: %ld with type: %d, and op at: %ld\n", (long) root, root->type, (long) root->content);
#   endif
    if (root == NULL) {
        return -1;
    }
    if (root->type == STR_LITP) {
        // NEED TO DEAL WITH THIS
        int label = add_to_literals(root->content);
        printf("L%d", label);
        return label;
    } else if (root->type == STRP) {
        // if it is a string we print it from the VarList
#       ifdef PGENOP
        printf("printing variable\n");
#       endif
        //print_Variable((Variable*) root->content);
        //print_from_VarList(root->content);
        Variable *var = get_latest_var(root->content);
        if (var != NULL) {
            if (var->function) {
                // NEED TO DEAL WITH FUNCTION CALL
            } else {
                print_Variable_location(var);
            }
        } else {
            printf("Variable %s is undefined\n", (char*) root->content);
            cleanup(13);
        }
#       ifdef PGENOP
        printf("done\n");
#       endif
        return 0;
    } else if (root->type == INTP) {
        // if it is just an int we print it
#       ifdef PGENOP
        printf("printing int\n");
#       endif
        print_int(*((int*) root->content), 0);
#       ifdef PGENOP
        printf("done\n");
#       endif
        return 0;
    } else if (root->type == DBLP) {
        // same with double
#       ifdef PGENOP
        printf("printing double\n");
#       endif
        print_double(*((double*) root->content), 0);
#       ifdef PGENOP
        printf("done\n");
#       endif
        return 0;
    } else if (root->type == UNIP) {
#       ifdef PGENOP
        printf("printing uni gate\n");
#       endif
        return print_Uni_gate((Uni_gate*) root->content);
#       ifdef PGENOP
        printf("done\n");
#       endif
    } else if (root->type == BINP) {
#       ifdef PGENOP
        printf("printing bin gate\n");
#       endif
        return print_Bin_gate((Bin_gate*) root->content);
#       ifdef PGENOP
        printf("done\n");
#       endif
    } else {
        printf("wrong operand type: %d\n", root->type);
        cleanup(3);
        return -1;
    }
}

void print_op(Operand *root) {
    // this is different than the above
    // and can be used to print the location of the gate
    // or the value contained in the operand within the Operand
    // which can be useful for debugging purposes
    if (root == NULL) {
        return;
    }
    if (root->type == STRP) {
        printf("%s\n", (char*) root->content);
    } else if (root-> type == INTP) {
        printf("%d\n", *((int*) root->content));
    } else if (root->type == DBLP) {
        printf("%f\n", *((double*) root->content));
    } else if (root->type == BINP) {
        printf("BIN at: %ld\n", (long) root->content);
    } else if (root->type == UNIP) {
        printf("UNI at: %ld\n", (long) root->content);
    } else if (root->type == DECP) {
        printf("DEC at: %ld\n", (long) root->content);
    }

}
