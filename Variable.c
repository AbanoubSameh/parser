// This file has been created on February-21-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "headers/LinkedList.h"
#include "headers/Variable.h"
#include "headers/functions.h"

void cleanup(int);

// these are the variable sizes
const int POINTER_SIZE = 8;
const int FUNCTION_SIZE = 1;
const int VAR_SIZES[] = {
    1,
    2,
    4,
    8,

    4,
    8,

    1
};

// variable names
const char *VAR_TYPE[] = {
    "byte",
    "short",
    "int",
    "long",

    "float",
    "double",

    "char",
};

// the available memory locations
int memory_locations = 0;

// the Variable constructor
// it gets a variable type, a name, and a level for the pointer like:
// int x is level 0 pointer
// int *x is level 1 pointer
// int **x is level 2 pointer
// and args for functions, can be used to other stuff later
Variable *new_Variable(int type, int pointer, int array, int function, char *name, void *scope_args) {
    // we allocate memory for the vairable
    Variable *var = malloc(sizeof(Variable));

    // all those we set normally from the arguments
    var->type = type;
    var->pointer = pointer;
    var->array = array;

    var->function = function;
    // Variables are not assigned by default
    var->assigned = 0;

    // strduplic used malloc internally and returns pointer to a COPY of the string
    // which we will need to free
    var->name = strduplic(name);
    // the scope can refer to an object of type Arguments for a function
    // which we do not need to duplicate, but need to destruct
    // meanwhile if it is a variable we will store the scope here
    // which also we do not need to duplicate but we need to take this into consideration
    var->scope_args = scope_args;
    var->memory_location =  get_memory_location(type, pointer, array, function, scope_args);
    return var;
}

// this gets a variable and returns its type
// it is useful for debugging
const char *get_Variable_type(Variable *var) {
    return get_var_type(var->type);
}

const char *get_var_type(int type) {
    return VAR_TYPE[type];
}

// this prints the vaiable location
// i.e. it gets a name (string) and generates
// the corresponding memory location
// this should normally be called by print_Operand
// during code generation
int print_Variable_location(Variable *var) {
    if (var == NULL) {
        printf("Variable does not exist\n");
        return -1;
    }
    if (var->function || (long) var->scope_args == 0) printf("L%d", var->memory_location);
    else printf("[sp, #-%d]", var->memory_location);
    return 0;
}

// this returns the size of a variable
// this should be used for something like sizeof
int get_Variable_size(Variable *var) {
    if (var->function) return FUNCTION_SIZE;
    // if variable is a pointer, then return pointer size
    if (var->pointer) return POINTER_SIZE;
    // else return the var size
    return VAR_SIZES[var->type];
}

// destructor for type Variable
void delete_Variable(Variable *var) {
    if (var == NULL) return;
#   ifdef PDEL
    printf("deleting a variable\n");
#   endif
    if (var->name != NULL) free(var->name);
    //if (var->var_name != NULL) free(var->args);
    free(var);
}

// this is used for debugging
// and it should print the type, location, size
// and name of a Variable
void print_var(Variable *var) {
    if (var == NULL) {
        printf("Variable does not exist\n");
        return;
    }
    char *global_label = "L";
    if (!var->function && (long) var->scope_args != 0) global_label = "";
    printf("Variable of type: %s", get_Variable_type(var));
    for (int i = 0; i < var->pointer; i++) {
        printf("*");
    }
    printf(" at location: %s%d and size %d and name: %s and is a function: %d and has args: %ld\n",
            global_label, var->memory_location, get_Variable_size(var), var->name, var->function, (long) var->scope_args);
}

// this functions should be used for memory allocation
int get_memory_location(int type, int pointer, int array, int function, void *scope) {
    // if it is a function or the variable is global return a label
    if (function || (long) scope == 0) {
        return get_label();
    }

    // else calculate the size of a single variable
    int single_size;
    if (pointer > 0) {
        // if it is a pointer then the size is of a pointer
        single_size = POINTER_SIZE;
    } else {
        // else it depends on the size of the variable
        single_size = VAR_SIZES[type];
    }
    if (array) memory_locations += single_size * array;
    else memory_locations += single_size;
    return memory_locations;
}
