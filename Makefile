
BIN=linux
BUILD=debug

DEFCC = gcc
ALTCC = clang
WINCC = x86_64-w64-mingw32-gcc

ifeq ($(BIN), linux)
	CC=$(DEFCC)
endif
ifeq ($(BIN), alternate)
	CC=$(ALTCC)
endif
ifeq ($(BIN), windows)
	CC=$(WINCC)
endif

DEFCFLAGS = -Wall -Wextra -g
RELEASECFLAGS = -Wall -Wextra -O3

ifeq ($(BUILD), debug)
	CFLAGS = $(DEFCFLAGS)
endif
ifeq ($(BUILD), release)
	CFLAGS = $(RELEASECFLAGS)
endif

DFLAGS = -DPGENBIN -DPGENUN -DPGENOP -DPGENNODE -DPGENLIST -DPGENVAR -DPDEL -DPGENVARFUN
PRINTFLAGS =
PRINT =

ifeq ($(PRINT), all)
	PRINTFLAGS = $(DFLAGS)
endif

LDFLAGS = -lm

PARSEOUT=no
YACCFLAGS =

ifeq ($(PARSEOUT), yes)
	YACCFLAGS = -v
endif

SRC = .
OBJ = obj
HDR = headers

MAINFILE=main.c
MAINFILE_OBJ = $(MAINFILE:.c=.o)
OUTFILE=front

FILES = $(OBJ)/$(MAINFILE_OBJ) $(OBJ)/parser.o $(OBJ)/lexer.o $(OBJ)/functions.o $(OBJ)/var_functions.o $(OBJ)/Operand.o $(OBJ)/Bin_gate.o $(OBJ)/Uni_gate.o $(OBJ)/Variable.o $(OBJ)/LinkedList.o $(OBJ)/Stack.o $(OBJ)/Argument.o $(OBJ)/Node.o $(OBJ)/Literal.o

#SRC_FILES = $(wildcard *.c)
#OBJ_FILES = $(OBJ)/$(SRC_FILES:.c=.o)

default: $(OBJ) front
	@echo "done building front"

front: $(SRC)/parser.c $(SRC)/lexer.c $(FILES)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(OUTFILE) $(FILES)

$(SRC)/parser.c: grammar.y
	yacc $(YACCFLAGS) -d -o $(SRC)/parser.c grammar.y
	mv $(SRC)/parser.h headers

$(SRC)/lexer.c: tokens.l
	lex -o $(SRC)/lexer.c tokens.l

$(OBJ)/%.o: $(SRC)/%.c
	$(CC) $(CFLAGS) $(PRINTFLAGS) -o $@ -c $(SRC)/$<

$(OBJ):
	@if [ ! -d $(OBJ) ]; then mkdir $(OBJ); echo "created $(OBJ) directory"; fi

clean:
	@rm -rf $(OBJ) $(SRC)/parser.c $(SRC)/parser.output $(SRC)/lexer.c $(HDR)/parser.h
	@echo "cleaned object files"

cleanall: clean
	@rm -f front front.exe
	@echo "removed binary"

rebuild: clean default
