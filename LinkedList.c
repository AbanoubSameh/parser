// This file has been created on March-22-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "headers/LinkedList.h"

// private function used by other functions
static int exists(LinkedList *list) {
    if (list != NULL) return 1;
    return 0;
}

static int has_one_Node(LinkedList *list) {
    if (list->root == list->last && list->root != NULL) return 1;
    return 0;
}

static int create_only_Node(LinkedList *list, void *obj) {
    list->root = new_Node(obj);
    list->last = list->root;
    list->num_of_nodes++;
    return 1;
}

static int delete_only_Node(LinkedList *list) {
    delete_Node(list->root, DELETE_OBJECT);
    list->root = NULL;
    list->last = NULL;
    list->num_of_nodes--;
    return 1;
}

// constructor for LinkedList
LinkedList *new_LinkedList() {
    LinkedList *list = malloc(sizeof(LinkedList));

    // initialize the values to NULL
    list->num_of_nodes = 0;
    list->root = NULL;
    list->last = NULL;
    //printf("a new list has been created\n");
    return list;
}

int delete_LinkedList(LinkedList *list) {
    if (!exists(list)) {
        printf("can't delete a list that does not exist\n");
        return 0;
    }

    printf("deleting a linked list\n");
	int count = 0;
	Node *temp = list->root;
    Node *temp2;

    // just deletes the list by deleting the Nodes
	while (temp != NULL) {
		temp2 = temp->next;
		//free(temp);
        delete_Node(temp, DELETE_OBJECT);
		temp = temp2;
		count++;
	}

    free(list);
	printf("%d Nodes freed\n", count);
    return 1;
}

int LinkedList_is_empty(LinkedList *list) {
    if (!exists(list)) {
        printf("can't find out if a list is empty if it does not exist\n");
        return 0;
    }
    if (list->root == NULL) {
        //printf("LinkedList is empty\n");
        return 1;
    }
    return 0;
}

int empty_LinkedList(LinkedList *list) {
    if (!exists(list)) {
        printf("can't empty a list that does not exist\n");
        return 0;
    }

    // just delete the list and return a new one
    delete_LinkedList(list);
    printf("a list has been freed\n");
    list = new_LinkedList();
    return 1;
}

// additional functions
int add_to_start_of_LinkedList(LinkedList *list, void *obj) {
    if (!exists(list)) {
        printf("can't add to a list that does not exist\n");
        return 0;
    }

    // if list is empty create first node
    if (LinkedList_is_empty(list)) return create_only_Node(list, obj);

    // else the first node becomes the new node
    Node *temp = list->root;
    list->root = new_Node(obj);

    // and the new node has old root as its next
    list->root->next = temp;
    list->num_of_nodes++;
    return 1;
}

int add_to_end_of_LinkedList(LinkedList *list, void *obj) {
    if (!exists(list)) {
        printf("can't add to a list that does not exist\n");
        return 0;
    }

    // if list is empty create first node
    if (LinkedList_is_empty(list)) return create_only_Node(list, obj);

    list->last->next = new_Node(obj);
    list->last = list->last->next;
    list->num_of_nodes++;
    return 1;
}

int remove_first_from_LinkedList(LinkedList *list) {
    if (!exists(list)) {
        printf("can't remove from a list that does not exist\n");
        return 0;
    }

    // if list is empty return NULL
    if (LinkedList_is_empty(list)) return 0;
    // if it has one node delete the node
    if (has_one_Node(list)) return delete_only_Node(list);

    // else delete root node and replace it with the next:

    // keep a pointer to root
    Node *temp = list->root;
    // set root to root's next
    list->root = list->root->next;
    // now delete the original root
    delete_Node(temp, DELETE_OBJECT);
    list->num_of_nodes--;
    return 1;
}

int remove_last_from_LinkedList(LinkedList *list) {
    if (!exists(list)) {
        printf("can't remove from a list that does not exist\n");
        return 0;
    }

    // if list is empty return NULL
    if (LinkedList_is_empty(list)) return 0;
    // if it has one node delete the node
    if (has_one_Node(list)) return delete_only_Node(list);

    // else delete the last node and replace it with the previous:

    Node *temp = list->root;
	while (temp->next->next != NULL) {
        // cycle until you reach the node just before last
        temp = temp->next;
	}

    // delete the node
    delete_Node(list->last, DELETE_OBJECT);
    // set the previous as the list's last node
    list->last = temp;
    // make sure to set the last node's last as NULL
    list->last->next = NULL;
    list->num_of_nodes--;
    return 1;
}

void *get_nth_object_in_LinkedList(LinkedList *list, int num) {
    if (!exists(list)) {
        printf("can't retrieve an object from a list that does not exist\n");
        return NULL;
    }

    // if it is empty just return NULL
    if (LinkedList_is_empty(list)) return NULL;

    if (num < 1) {
        printf("number must be greater than 0\n");
        return NULL;
    }

    if (num > list->num_of_nodes) {
        printf("list has only %d objects, object %d can't be retrieved\n", list->num_of_nodes, num);
        return NULL;
    }

    Node *temp = list->root;
	for (int i = 0; i < num - 1; i++) {
        // cycle until you reach the nth node
        temp = temp->next;
	}

    return temp->obj;
}

void *get_first_object_in_LinkedList(LinkedList *list) {
    return get_nth_object_in_LinkedList(list, 1);
}

void *get_last_object_in_LinkedList(LinkedList *list) {
    return get_nth_object_in_LinkedList(list, list->num_of_nodes);
}

int remove_nth_object_in_LinkedList(LinkedList *list, int num) {
    if (!exists(list)) {
        printf("can't remove an object from a list that does not exist\n");
        return 0;
    }

    // if it is empty just return NULL
    if (LinkedList_is_empty(list)) return 0;

    if (num < 1) {
        printf("number must be greater than 0\n");
        return 0;
    }

    if (num > list->num_of_nodes) {
        printf("list has only %d objects, object %d can't be removed\n", list->num_of_nodes, num);
        return 0;
    }

    if (num == 1) {
        // if the number is 1, you just make the 2nd node the new root
        Node *toDelete = list->root;
        list->root = list->root->next;
        delete_Node(toDelete, DELETE_OBJECT);
    } else if (num == list->num_of_nodes) {
        // if it is equal to the number of noded, just drop the last node
        Node *toDelete = list->last;
        list->last = NULL;
        delete_Node(toDelete, DELETE_OBJECT);
    } else {
        // else cycle until you reach the node before the wanted node
        Node *temp = list->root;
        for (int i = 0; i < num - 2; i++) {
            // cycle until you reach the nth node
            temp = temp->next;
        }
        // make the node's next the one after the wanted node
        Node *toDelete = temp->next;
        temp->next = temp->next->next;
        delete_Node(toDelete, DELETE_OBJECT);
    }

    list->num_of_nodes--;
    return 1;
}
