%{
#include <stdio.h>
#include <stdlib.h>
#include "headers/Operand.h"
#include "headers/Bin_gate.h"
#define YYSTYPE Operand*
#include "headers/parser.h"
void cleanup(int);

typedef union {
    int num;
    double dbl;
} Temp;

Temp temp;
int line_count = 1;
int letter_count = 1;

%}

%option noinput
%option nounput

%%

"\"".*"\""                  { letter_count += yyleng; yylval = new_Operand(yytext, STR_LITP); return STR_LIT; }

"void"                      { letter_count += yyleng; return VID_ID; }
"int"                       { letter_count += yyleng; return INT_ID; }
"double"                    { letter_count += yyleng; return DBL_ID; }
"char"                      { letter_count += yyleng; return CHR_ID; }

"if"                        { letter_count += yyleng; return IF; }
"else"                      { letter_count += yyleng; return ELSE; }
"for"                       { letter_count += yyleng; return FOR; }
"while"                     { letter_count += yyleng; return WHILE; }
"do"                        { letter_count += yyleng; return DO; }

"+"                         { letter_count += yyleng; return ADD; }
"-"                         { letter_count += yyleng; return SUB; }
"*"                         { letter_count += yyleng; return MUL; }
"/"                         { letter_count += yyleng; return DIV; }
"%"                         { letter_count += yyleng; return MOD; }

"("                         { letter_count += yyleng; return OPR; }
")"                         { letter_count += yyleng; return CPR; }
"{"                         { letter_count += yyleng; return OBR; }
"}"                         { letter_count += yyleng; return CBR; }
"["                         { letter_count += yyleng; return OSB; }
"]"                         { letter_count += yyleng; return CSB; }
"->"                        { letter_count += yyleng; return ARR; }
"."                         { letter_count += yyleng; return DOT; }
","                         { letter_count += yyleng; return CMA; }
";"                         { letter_count += yyleng; return SMC; }

"&&"                        { letter_count += yyleng; return AND; }
"\\"                        { letter_count += yyleng; return ORR; }

"&"                         { letter_count += yyleng; return BND; }
"|"                         { letter_count += yyleng; return BOR; }
"^"                         { letter_count += yyleng; return BXR; }

"=="                        { letter_count += yyleng; return EQU; }
"!="                        { letter_count += yyleng; return NEQ; }

"<"                         { letter_count += yyleng; return STH; }
">"                         { letter_count += yyleng; return GTH; }
"<="                        { letter_count += yyleng; return SEQ; }
">="                        { letter_count += yyleng; return GEQ; }

"<<"                        { letter_count += yyleng; return SHL; }
">>"                        { letter_count += yyleng; return SHR; }
"<<<"                       { letter_count += yyleng; return ROL; }
">>>"                       { letter_count += yyleng; return ROR; }

"~"                         { letter_count += yyleng; return TIL; }
"!"                         { letter_count += yyleng; return NOT; }

"++"                        { letter_count += yyleng; return INC; }
"--"                        { letter_count += yyleng; return DEC; }

"="                         { letter_count += yyleng; return ASS; }
"+="                        { letter_count += yyleng; return PLA; }
"-="                        { letter_count += yyleng; return MIA; }
"*="                        { letter_count += yyleng; return MUA; }
"/="                        { letter_count += yyleng; return DIA; }
"%="                        { letter_count += yyleng; return MOA; }
"<<="                       { letter_count += yyleng; return SLA; }
">>="                       { letter_count += yyleng; return SRA; }
"<<<="                      { letter_count += yyleng; return RLA; }
">>>="                      { letter_count += yyleng; return RRA; }
"&="                        { letter_count += yyleng; return ANA; }
"|="                        { letter_count += yyleng; return ORA; }
"^="                        { letter_count += yyleng; return NOA; }

[0-9]*\.[0-9]+              { letter_count += yyleng; temp.dbl = atof(yytext);
                                yylval = new_Operand(&temp.dbl, DBLP); return DBL; }
[0-9]+                      { letter_count += yyleng; temp.num = atoi(yytext);
                                yylval = new_Operand(&temp.dbl, INTP); return INT; }
0[xX][0-9a-fA-F]+           { letter_count += yyleng; temp.num = (int) strtol(yytext, NULL, 0);
                                yylval = new_Operand(&temp.dbl, INTP); return INT; }
[0-9a-fA-F]+[Hh]            { letter_count += yyleng; temp.num = (int) strtol(yytext, NULL, 16);
                                yylval = new_Operand(&temp.dbl, INTP); return INT; }
0[bB][01]+                  { letter_count += yyleng; temp.num = (int) strtol(yytext+2, NULL, 2);
                                yylval = new_Operand(&temp.dbl, INTP); return INT; }
[01]+[Bb]                   { letter_count += yyleng; temp.num = (int) strtol(yytext, NULL, 2);
                                yylval = new_Operand(&temp.dbl, INTP); return INT; }
[_$a-zA-Z][_$a-zA-Z0-9]*    { letter_count += yyleng; yylval = new_Operand(yytext, STRP); return STR; }

<<EOF>>                     { return END; }
[\n]                        { line_count++; letter_count = 1; }
[ \t]                       { letter_count += yyleng; }
[^0-9]                      { printf("wrong input: \"%c\"\tbin: %d at line: %d and letter: %d\n",
                                yytext[0], yytext[0], line_count, letter_count); cleanup(1); }

%%
