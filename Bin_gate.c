// This file has been created on February-09-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "headers/functions.h"
#include "headers/var_functions.h"
#include "headers/Operand.h"
#include "headers/Bin_gate.h"
#include "headers/Uni_gate.h"
#include "headers/Variable.h"
#include "headers/Stack.h"
#include "headers/Argument.h"

void cleanup(int);
extern Stack *scope;

// Bin_gate constructor with operands and type
Bin_gate* new_cBin_gate(Operand *op1, Operand *op2, uint8_t type) {
    Bin_gate *gate = malloc(sizeof(Bin_gate));
    gate->type = type;
    gate->operand1 = op1;
    gate->operand2 = op2;
#   ifdef PGENBIN
    //printf("gate created at: %ld with type: %s, op1: ", (long) gate, get_token(type));
    //print_op(op1);
    //printf(", op2: ");
    //print_op(op2);
    //printf("\n");
    printf("gate created at: %ld with type: %s, op1 at: %ld, and op2 at: %ld\n", (long) gate, get_token(type), (long) op1, (long) op2);
#   endif
    return gate;
}

// Bin_gate constructor with no arguments
Bin_gate* new_Bin_gate() {
    Bin_gate *gate = malloc(sizeof(Bin_gate));
    gate->operand1 = NULL;
    gate->operand2 = NULL;
#   ifdef PGENBIN
    printf("gate created at: %ld\n", (long) gate);
#   endif
    return gate;
}

// setters for the Bin_gate
void set_Bgate_type(Bin_gate *gate, uint8_t type) {
    gate->type = type;
#   ifdef PGENBIN
    printf("gate at %ld has type: %d\n", (long) gate, type);
#   endif
}

void set_Bgate_operand1(Bin_gate *gate, Operand *op) {
    gate->operand1 = op;
#   ifdef PGENBIN
    printf("gate at %ld has op1: %ld\n", (long) gate, (long) op);
#   endif
}

void set_Bgate_operand2(Bin_gate *gate, Operand *op) {
    gate->operand2 = op;
#   ifdef PGENBIN
    printf("gate at %ld has op2: %ld\n", (long) gate, (long) op);
#   endif
}

// destructor for Bin_gate
void delete_Bin_gate(Bin_gate *gate) {
    // SHOULD MAKE SURE THESE EXIST
    // AND/OR CHECK THAT THE gate IS NOT NULL
    if (gate == NULL) return;
#   ifdef PDEL
    printf("deleting a bin_gate\n");
#   endif
    //print_op(gate->operand1);
    if (gate->operand1 != NULL) delete_Operand(gate->operand1);
    //print_op(gate->operand1);
    if (gate->operand2 != NULL) delete_Operand(gate->operand2);
    free(gate);
}

// THOSE FUNCTIONS CONTAIN MANY PARTS THAT NEED TO GO INTO
// THEIR OWN FUNCIONS AND BE REUSED
int solve_and_gate(Bin_gate *root) {
    int state = 0, reg1 = -1, reg2 = -1;
    int label1 = get_label(), label2 = get_label();

    if (root->operand1->type == BINP || root->operand1->type == UNIP) {
        reg1 = print_Operand(root->operand1);
        //printf("reg1: %d\n", reg1);
        state = 1;
    }
    if (root->operand2->type == BINP || root->operand2->type == UNIP) {
        reg2 = print_Operand(root->operand2);
        //printf("reg2: %d\n", reg2);
        if (state == 0) {
            state = 2;
        } else {
            state = 3;
        }
    }

    if (state == 0) {
        reg1 = get_register();

        print_uni_op_inst(MOVM, reg1, REG, 2);
        print_Operand(root->operand1);
        printf("\n");

        print_bin_op_inst(CMPM, reg1, REG, 0, NUM, 1);
        print_uni_op_inst(JEM, label1, LBL, 1);

        print_uni_op_inst(MOVM, reg1, REG, 2);
        print_Operand(root->operand2);
        printf("\n");

        print_bin_op_inst(CMPM, reg1, REG, 0, NUM, 1);
        print_uni_op_inst(JEM, label1, LBL, 1);

        print_bin_op_inst(MOVM, reg1, REG, 1, NUM, 1);
        print_uni_op_inst(JMPM, label2, LBL, 1);

        print_label(label1, 1);
        print_bin_op_inst(MOVM, reg1, REG, 0, NUM, 1);

        print_label(label2, 1);
    } else if (state == 1) {
        print_bin_op_inst(CMPM, reg1, REG, 0, NUM, 1);
        print_uni_op_inst(JEM, label1, LBL, 1);

        print_uni_op_inst(MOVM, reg1, REG, 2);
        print_Operand(root->operand2);
        printf("\n");

        print_bin_op_inst(CMPM, reg1, REG, 0, NUM, 1);
        print_uni_op_inst(JEM, label1, LBL, 1);

        print_bin_op_inst(MOVM, reg1, REG, 1, NUM, 1);
        print_uni_op_inst(JMPM, label2, LBL, 1);

        print_label(label1, 1);
        print_bin_op_inst(MOVM, reg1, REG, 0, NUM, 1);

        print_label(label2, 1);
    } else if (state == 2) {
        print_bin_op_inst(CMPM, reg2, REG, 0, NUM, 1);
        print_uni_op_inst(JEM, label1, LBL, 1);

        print_uni_op_inst(MOVM, reg2, REG, 2);
        print_Operand(root->operand1);
        printf("\n");

        print_bin_op_inst(CMPM, reg2, REG, 0, NUM, 1);
        print_uni_op_inst(JEM, label1, LBL, 1);

        print_bin_op_inst(MOVM, reg2, REG, 1, NUM, 1);
        print_uni_op_inst(JMPM, label2, LBL, 1);

        print_label(label1, 1);
        print_bin_op_inst(MOVM, reg2, REG, 0, NUM, 1);

        print_label(label2, 1);
    } else  if (state == 3) {
        print_bin_op_inst(CMPM, reg1, REG, 0, NUM, 1);
        print_uni_op_inst(JEM, label1, LBL, 1);

        print_bin_op_inst(CMPM, reg2, REG, 0, NUM, 1);
        print_uni_op_inst(JEM, label1, LBL, 1);

        free_register(reg2);

        print_bin_op_inst(MOVM, reg1, REG, 1, NUM, 1);
        print_uni_op_inst(JMPM, label2, LBL, 1);

        print_label(label1, 1);
        print_bin_op_inst(MOVM, reg1, REG, 0, NUM, 1);

        print_label(label2, 1);
    } else {
        printf("Unknown error\n");
        cleanup(4);
        return -1;
    }

    return reg1;
}

int solve_or_gate(Bin_gate *root) {
    int state = 0, reg1 = -1, reg2 = -1;
    int label1 = get_label(), label2 = get_label(), label3 = get_label();

    if (root->operand1->type == BINP || root->operand1->type == UNIP) {
        reg1 = print_Operand(root->operand1);
        //printf("reg1: %d\n", reg1);
        state = 1;
    }
    if (root->operand2->type == BINP || root->operand2->type == UNIP) {
        reg2 = print_Operand(root->operand2);
        //printf("reg2: %d\n", reg2);
        if (state == 0) {
            state = 2;
        } else {
            state = 3;
        }
    }

    if (state == 0) {
        reg1 = get_register();

        print_uni_op_inst(MOVM, reg1, REG, 2);
        print_Operand(root->operand1);
        printf("\n");

        print_bin_op_inst(CMPM, reg1, REG, 0, NUM, 1);
        print_uni_op_inst(JNEM, label1, LBL, 1);

        print_uni_op_inst(MOVM, reg1, REG, 2);
        print_Operand(root->operand2);
        printf("\n");

        print_bin_op_inst(CMPM, reg1, REG, 0, NUM, 1);
        print_uni_op_inst(JNEM, label1, LBL, 1);

        print_uni_op_inst(JMPM, label2, LBL, 1);

        print_label(label1, 1);
        print_bin_op_inst(MOVM, reg1, REG, 1, NUM, 1);
        print_uni_op_inst(JMPM, label3, LBL, 1);

        print_label(label2, 1);
        print_bin_op_inst(MOVM, reg1, REG, 0, NUM, 1);

        print_label(label3, 1);
    } else if (state == 1) {
        print_bin_op_inst(CMPM, reg1, REG, 0, NUM, 1);
        print_uni_op_inst(JNEM, label1, LBL, 1);

        print_uni_op_inst(MOVM, reg1, REG, 2);
        print_Operand(root->operand2);
        printf("\n");

        print_bin_op_inst(CMPM, reg1, REG, 0, NUM, 1);
        print_uni_op_inst(JNEM, label1, LBL, 1);

        print_uni_op_inst(JMPM, label2, LBL, 1);

        print_label(label1, 1);
        print_bin_op_inst(MOVM, reg1, REG, 1, NUM, 1);
        print_uni_op_inst(JMPM, label3, LBL, 1);

        print_label(label2, 1);
        print_bin_op_inst(MOVM, reg1, REG, 0, NUM, 1);

        print_label(label3, 1);
    } else if (state == 2) {
        print_bin_op_inst(CMPM, reg2, REG, 0, NUM, 1);
        print_uni_op_inst(JNEM, label1, LBL, 1);

        print_uni_op_inst(MOVM, reg2, REG, 2);
        print_Operand(root->operand1);
        printf("\n");

        print_bin_op_inst(CMPM, reg2, REG, 0, NUM, 1);
        print_uni_op_inst(JNEM, label1, LBL, 1);

        print_uni_op_inst(JMPM, label2, LBL, 1);

        print_label(label1, 1);
        print_bin_op_inst(MOVM, reg2, REG, 1, NUM, 1);
        print_uni_op_inst(JMPM, label3, LBL, 1);

        print_label(label2, 1);
        print_bin_op_inst(MOVM, reg2, REG, 0, NUM, 1);

        print_label(label3, 1);
    } else  if (state == 3) {
        print_bin_op_inst(CMPM, reg1, REG, 0, NUM, 1);
        print_uni_op_inst(JNEM, label1, LBL, 1);

        print_bin_op_inst(CMPM, reg2, REG, 0, NUM, 1);
        print_uni_op_inst(JNEM, label1, LBL, 1);

        free_register(reg2);
        print_uni_op_inst(JMPM, label2, LBL, 1);

        print_label(label1, 1);
        print_bin_op_inst(MOVM, reg1, REG, 1, NUM, 1);
        print_uni_op_inst(JMPM, label3, LBL, 1);

        print_label(label2, 1);
        print_bin_op_inst(MOVM, reg1, REG, 0, NUM, 1);

        print_label(label3, 1);
    } else {
        printf("Unknown error\n");
        cleanup(4);
        return -1;
    }

    return reg1;
}

Variable *solve_declaration(Bin_gate *root) {
    //printf("solving declaration\n");
    int type = *((int*) root->operand1->content);
    Operand *temp = root->operand2;
    int pointer = 0;
    int array = 0;
    int function = 0;
    void *scope_args = peek(scope);
    while (temp->type != STRP) {
        if (temp->type == BINP) {
            if (((Bin_gate*) temp->content)->type == ARRAYM) {
                // NEED TO DEAL WITH THESE TOO
                printf("looks like we have an array\n");
                if (((Bin_gate*) temp->content)->operand2 == NULL) {
                    array = 0;
                } else {
                    array = *(int*) ((Bin_gate*) temp->content)->operand2->content;
                }
            } else if (((Bin_gate*) temp->content)->type == ARGS) {
                // NEED TO DEAL WITH THESE TOO
                printf("looks like we have a function declaration\n");
                function = 1;
                if (((Bin_gate*) temp->content)->operand2 == NULL) {
                    scope_args = NULL;
                } else {
                    // NEED TO SOLVE ARGUMENTS
                    // array = *(int*) ((Bin_gate*) temp->content)->operand2->content;
                }
            }
            temp = ((Bin_gate*) temp->content)->operand1;
        } else if (temp->type == UNIP) {
            if (((Uni_gate*) temp->content)->type == POINTM) {
                temp = ((Uni_gate*) temp->content)->operand1;
                pointer++;
            } else if (((Uni_gate*) temp->content)->type == REFM) {
                printf("Can't have a variable with an ampersand\n");
                cleanup(12);
            }
        }
    }
    char *name = (char*) temp->content;
    //printf("%ld\n", (long) root);
    //printf("declaration with type: %d\n", type);
    //printf("and name: %s\n", name);
    //printf("and pointer: %d\n", pointer);
    //printf("and array: %d\n", array);
    //printf("and is function: %d\n", function);
    //printf("and scope/args: %ld\n", (long) scope_args);
    return add_to_varList(type, pointer, array, function, name, scope_args);
}

int solve_assignment(Bin_gate *root) {
    //printf("solving assignment\n");
    if (root->operand1->type == BINP) {
        Variable *var = solve_declaration((Bin_gate*) root->operand1->content);
        printf("\t\t%s\t\t", get_token(MOVM));
        print_Variable_location(var);
        printf(",\t\t");
        print_Operand(root->operand2);
        printf("\n");
    } else {
        printf("\t\t%s\t\t", get_token(MOVM));
        print_Operand(root->operand1);
        printf(",\t\t");
        print_Operand(root->operand2);
        printf("\n");
    }
    return -1;
}

int solve_func(Bin_gate *root) {
    if ((long) get_value_at_base(scope) == 1) {
        printf("can't have a function inside a function\n");
        cleanup(9);
    }
    push(scope, new_Operand(root->operand1->content, STRP));
    printf("the scope has been changed to %s\n", (char*) root->operand1->content);
    //Arguments *args = new_Arguments(root->operand2);
    //add_to_VarList(INT_TYPE, root->operand1, 0, root->operand2);
    //printf("%s:\n", (char*) root->operand1->op);
    return -1;
}

int print_Bin_gate(Bin_gate *root) {
    //printf("solving bin gate\n");
    int state = 0, reg1 = -1, reg2 = -1;

    // special cases
    if (root->type == ORM) return solve_or_gate(root);
    else if (root->type == ANDM) return solve_and_gate(root);
    else if (root->type == ASSM) return solve_assignment(root);
    else if (root->type == FUNC) return solve_func(root);

    // if the first or second operand contains another gate, then
    // print it first
    if (root->operand1->type == BINP || root->operand1->type == UNIP) {
        reg1 = print_Operand(root->operand1);
        state = 1;
    }
    if (root->operand2->type == BINP || root->operand2->type == UNIP) {
        reg2 = print_Operand(root->operand2);
        if (state == 0) state = 2;
        else state = 3;
    }

    if (state == 0) {
        // if no gates printed then get a new register
        reg1 = get_register();
        // move into it the first value and then print the
        // next operations

        print_uni_op_inst(MOVM, reg1, REG, 2);
        print_Operand(root->operand1);
        printf("\n");

        print_uni_op_inst(root->type, reg1, REG, 2);
        print_Operand(root->operand2);
        printf("\n");
    } else if (state == 1) {
        // if only first gate printed
        // do the operations to the returned register
        print_uni_op_inst(root->type, reg1, REG, 2);
        print_Operand(root->operand2);
        printf("\n");
    } else if (state == 2) {
        // same thing with the second
        print_uni_op_inst(root->type, reg2, REG, 2);
        print_Operand(root->operand1);
        printf("\n");
        return reg2;
    } else if (state == 3) {
        // if the two were printed just do an operation
        // that involves the two
        print_bin_op_inst(root->type, reg1, REG, reg2, REG, 1);
        // and free the second register
        free_register(reg2);
    } else {
        printf("Unknown error\n");
        cleanup(4);
        return -1;
    }

    return reg1;
}
